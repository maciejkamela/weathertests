module.exports = function (grunt) {
    'use strict';

    var configOptions = {
        config: {
            src: "./uat_tests/config/grunt/*.js"
        },
        protractor: {
            cucumber: {
                options: {
                    configFile: 'uat_tests/config/protractor.cucumber.conf.js',
                    testJSONDirectory: 'reports/json-report.json',
                    keepAlive: true
                }
            }
        },
        'protractor-cucumber-html-report': {
            default_options: {
                options: {
                    dest: 'uat_tests/reports',
                    output: 'report.html',
                    testJSONDirectory: 'uat_tests/reports/json',
                    testJSONResultPath: 'reports/json/report.json'
                }
            }
        }
    };

    var configs = require('load-grunt-configs')(grunt, configOptions);
    grunt.loadTasks('uat_tests/config/grunt/tasks');
    require('load-grunt-tasks')(grunt);
    grunt.initConfig(configs);
};
