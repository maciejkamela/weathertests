module.exports = function (grunt) {
    grunt.registerTask('e2e', 'Generate e2e functional tests.', function () {
        var DEFAULT_ENV = 'main',
            //usersJSONPath,
            seleniumAddress,
            params = {
                envToRun: arguments[0] || DEFAULT_ENV,
                generateReport: arguments[1] === 'true' ? true : false
            };
        if (process.argv.indexOf('--info') !== -1) {
            e2eInfo();
            return;
        }

        //usersJSONPath = getUsersJsonPath(params.envToRun);
        seleniumAddress = getSeleniumAddress();

        generateE2Etest(grunt, params.envToRun, params.generateReport, seleniumAddress);
    });

        grunt.registerTask('runTestsWithTag', 'Run report with tag.', getTag);

        function generateE2Etest(grunt, envName, withReport, seleniumAddress) {
            var tasks;
            grunt.option('protractorEnv', envName);
            grunt.option('format', 'pretty');
            //grunt.option('usersJSONPath', usersJSONPath);
            grunt.option('seleniumAddress', seleniumAddress);
            //tasks = ['template:protractor'];
            tasks = ['template:protractor', 'runTestsWithTag', 'jshint:protractor', 'clean:e2eReport', 'protractor'];
            if (withReport === true) {
                grunt.option('format', 'json');
                tasks.push('protractor-cucumber-html-report');
                grunt.log.writeln("I am launching tests on " + envName + " environment with report using selenium " + seleniumAddress);
            } else {
                grunt.log.writeln("I am launching tests on " + envName + " environment without report using selenium " + seleniumAddress);
            }
            grunt.task.run(tasks);
        }

        function e2eInfo() {
            var testEnv = grunt.file.readJSON('./uat_tests/config/test_env.json'),
                protractorEnv = grunt.option('protractorEnv'),
                availableEnv = Object.keys(testEnv),
                path = './uat_tests/tests/features/',
                filePattern = /^.*\.(feature)$/;
            grunt.log.writeln("*********\nPlease use one of the beneath listed environment: \n" + availableEnv.sort().join(", ") + "\n*********");
            grunt.log.writeln("Please use one of the following tag:");
            grunt.file.recurse(path, function (abspath, rootdir, subdir, filename) {
                var feature = grunt.file.read(abspath),
                    pattern = /(^|\n| )[@][a-zA-Z0-9]+/g,
                    featureTags = feature.match(pattern),
                    uniqueTags;
                    if (filePattern.test(filename) && featureTags !== null) {
                        uniqueTags = eliminateDuplicates(featureTags).join(", ");
                        grunt.log.writeln(filename.replace(/.feature$/, '') + ": ", uniqueTags);
                    }
                });
        }

        function eliminateDuplicates(arr) {
            var i,
                itemsAmount = arr.length,
                uniqueItems = [];
            for (i = 0; i < itemsAmount; i++) {
                if (uniqueItems.indexOf(arr[i]) === -1)
                    uniqueItems.push(arr[i]);
            }
            return uniqueItems;
        }

        function getSeleniumAddress() {
            if (process.argv.indexOf('--grid') !== -1) {
                return 'http://10.157.50.219:4444/wd/hub';
            } else {
                return 'http://127.0.0.1:4444/wd/hub';
            }
        }

        //function getUsersJsonPath(env) {
        //    switch (env) {
        //        case "stage":
        //            return '../tests/support/data/usersStage.json';
        //        case "qual":
        //            return '../tests/support/data/usersQual.json';
        //        default:
        //            return '../tests/support/data/usersQual.json';
        //    }
        //}

        function getTag() {
            var gruntOptionPattern = /--@[a-z]*/g,
                gruntParams = process.argv,
                index = [],
                fs = require('fs'),
                config = './uat_tests/config/protractor.cucumber.conf.js',
                configElemPattern = /format: \'[a-z]*\'[,]/g;
            gruntParams.forEach(function (val, i) {
                if (val.match(gruntOptionPattern) !== null) {
                    index.push(i);
                }
            });
            var tagWithDashes = process.argv[index];

            if (tagWithDashes) {
                var tag = tagWithDashes.substring(2),
                    configContent = grunt.file.read('./uat_tests/config/protractor.cucumber.conf.js'),
                    elemToReplace = configContent.match(configElemPattern);
                configContent = configContent.replace(
                    elemToReplace,
                    elemToReplace + '\n' + '\t\t' + "tags: [" + '\'' + tag + '\'' + "],"
                );
                fs.writeFileSync(config, configContent);
            }
        }
    };