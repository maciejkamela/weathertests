///**
// * Created by camel on 2016-03-07.
// */
//module.exports = function (grunt) {
//    grunt.registerTask('e2e', 'Generate e2e functional tests.', function () {
//        var argumentsAmount = arguments.length,
//            DEFAULT_ENV = 'main',
//            DEFAULT_TAG = '@pogoda',
//            envName,
//            tagName,
//            withReport;
//        if (process.argv.indexOf('--info') !== -1) {
//            e2eInfo();
//            return;
//        }
//        switch (argumentsAmount) {
//            case 0:
//                envName = DEFAULT_ENV;
//                tagName = DEFAULT_TAG;
//                withReport = false;
//                console.log('dooooooopa');
//                generateE2Etest(grunt, envName, tagName, withReport);
//                break;
//            case 1:
//                if (arguments[0] === 'true') {
//                    envName = DEFAULT_ENV;
//                    tagName = DEFAULT_TAG;
//                    withReport = true;
//                    console.log('z raportem');
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                } else if (arguments[0].indexOf('@') === 0) {
//                    envName = DEFAULT_ENV;
//                    tagName = arguments[0];
//                    withReport = false;
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                } else {
//                    envName = arguments[0];
//                    tagName = DEFAULT_TAG;
//                    withReport = false;
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                }
//                break;
//            case 2:
//                if (arguments[0].indexOf('@') === -1 && arguments[1].indexOf('@') === 0) {
//                    envName = arguments[0];
//                    tagName = arguments[1];
//                    withReport = false;
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                }
//                else if (arguments[0].indexOf('@') === 0 && arguments[1] === 'true') {
//                    envName = DEFAULT_ENV;
//                    tagName = arguments[0];
//                    withReport = true;
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                }
//                else {
//                    envName = arguments[0];
//                    tagName = DEFAULT_TAG;
//                    withReport = true;
//                    generateE2Etest(grunt, envName, tagName, withReport);
//                }
//                break;
//            case 3:
//                envName = arguments[0];
//                tagName = arguments[1];
//                withReport = (arguments[2] === 'true');
//                generateE2Etest(grunt, envName, tagName, withReport);
//                break;
//            default:
//                grunt.log.writeln("Please make sure that arguments order is correct: grunt e2e:env:tag:withReport. " +
//                    "You can skip one or more arguments but their order must be preserved.");
//        }
//    });
//
//    function generateE2Etest(grunt, envName, tagName, withReport) {
//        var tasks;
//        grunt.option('protractorEnv', envName);
//        grunt.option('format', 'pretty');
//        grunt.option('tagName', tagName);
//        tasks = ['clean','jshint','protractor'];
//        if (withReport === true) {
//            grunt.option('format', 'json');
//            tasks.push('protractor-cucumber-html-report');
//            grunt.log.writeln("I am launching tests tagged with " + tagName + " on " + envName + " environment with report.");
//        } else {
//            grunt.log.writeln("I am launching tests tagged with " + tagName + " on " + envName + " environment without report.");
//        }
//        grunt.task.run(tasks);
//    }
//    function e2eInfo() {
//        var testEnv = grunt.file.readJSON('./uat_tests/config/test_env.json'),
//            protractorEnv = grunt.option('protractorEnv'),
//            availableEnv = Object.keys(testEnv),
//            path = './uat_tests/tests',
//            filePattern = /^.*\.(feature)$/;
//        grunt.log.writeln("*********\nPlease use one of the beneath listed environment: \n" + availableEnv.sort().join(", ") + "\n*********");
//        grunt.log.writeln("Please use one of the following tag:");
//        grunt.file.recurse(path, function (abspath, rootdir, subdir, filename) {
//            var feature = grunt.file.read(abspath),
//                pattern = /(^|\n| )[@][a-zA-Z0-9]+/g,
//                featureTags = feature.match(pattern),
//                uniqueTags;
//            if (filePattern.test(filename)) {
//                uniqueTags = eliminateDuplicates(featureTags).join(", ");
//                grunt.log.writeln(filename.replace(/.feature$/, '') + ": ", uniqueTags);
//            }
//        });
//    }
//    function eliminateDuplicates(arr) {
//        var i,
//            itemsAmount = arr.length,
//            uniqueItems = [];
//        for (i = 0; i < itemsAmount; i++) {
//            if (uniqueItems.indexOf(arr[i]) === -1)
//                uniqueItems.push(arr[i]);
//        }
//        return uniqueItems;
//    }
//};