// Empties folders to start fresh
module.exports = function() {
  'use strict';
  return {
    e2eReport: {
        src: ['./uat_tests/reports']
  }
  };
};