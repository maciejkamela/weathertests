var extend = require('extend');
module.exports = function (grunt, options) {
    'use strict';
    var data = {};
    return {
        options: {
            data: data
        },
        protractor: {
            options: {
                data: function () {
                    var testEnv = require('../test_env.json'),
                        protractorEnv = grunt.option('protractorEnv'),
                        availableEnv = Object.keys(testEnv);
                    if (testEnv[protractorEnv]) {
                        data = extend(true, data, testEnv[protractorEnv]);
                        data.format = grunt.option('format');
                        console.log(data);
                        data.seleniumAddress = grunt.option('seleniumAddress');
                    } else {
                        data = null;
                        grunt.fail.fatal('There is no ' + protractorEnv + ' env. ' + 'Please use one of the following: ' + availableEnv + '. \n' +
                            'Make sure that arguments order is correct: grunt e2e:env:tag:withReport.');
                    }

                    return data;
                }
            },
            files: [
                {'uat_tests/config/protractor.cucumber.conf.js': ['uat_tests/config/protractor.cucumber.conf.template.js']}
            ]
        }
    };
};