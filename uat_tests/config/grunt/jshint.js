module.exports = function() {
  'use strict';

  return {

    options: {
      jshintrc: '.jshintrc',
      reporter: require('jshint-stylish')
    },
    protractor: {
      options: {
        jshintrc: '.jshintrcProtractor'
      },
      src: [
        'uat_tests/tests/{,**/}*.js'
      ]
    }
  };
};