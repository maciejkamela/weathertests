'use strict';
var world = (function () {
    var chai = require('chai'),
        chaiAsPromised = require("chai-as-promised");
    chai.use(chaiAsPromised);

    var expect = chai.expect,
        EC = require('protractor').ExpectedConditions,
        support = require('../tests/support/methods/basicSupport.js');

    return {
        expect: expect,
        EC: EC,
        support: support
    };
}());
module.exports = world;