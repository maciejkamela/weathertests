
console.log(this.options);
config = require('../config/protractor.cucumber.conf.js');

exports.config = {
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    seleniumArgs: [],
    //seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    seleniumAddress: '<%= seleniumAddress %>',
    jsonReportsDirOutput: 'uat_tests/reports/json',
    baseUrl: '<%= baseUrl %>',
    //baseUrl: 'http://presdszone1.e-wsi.com',

    onPrepare: function() {
        browser.driver.manage().window().maximize();
        global.dvr = browser.driver;
        //TODO:
        // This will tell the Protractor not to wait for angular
        //global.isAngularSite = function(flag) {
        //    browser.ignoreSynchronization = !flag;
        //};
        browser.manage().timeouts().pageLoadTimeout(40000);
        browser.manage().timeouts().implicitlyWait(25000);
        // for non-angular page
        browser.ignoreSynchronization = true;
    },


    verbose: true,

    specs: ['../tests/features/*.feature'],

    multiCapabilities: [

        {
            'browserName': 'firefox',
            'tunnel-identifier': process.env.TRAVIS_JOB_NUMBER,

            'build': 12345,

            'name': 'Firefox - Weather tests.'
        },

        {
            'browserName': 'internet explorer',
            'tunnel-identifier': process.env.TRAVIS_JOB_NUMBER,

            'build': 12345,

            'name': 'IE - Weather tests.'
        },

        {
            'browserName': 'chrome',
            'chromeOptions': {
                'args': ['--test-type', 'show-fps-counter=true']
            },
            'tunnel-identifier': process.env.TRAVIS_JOB_NUMBER,

            'build': 12345,

            'name': 'Chrome - Weather tests.'
        }

    ],
    cucumberOpts: {
        format: '<%= format %>',
        require: ['../tests/step_definitions/*.js','../support/*.js']
        //require: ['../tests/step_definitions/*.js']
    },

    // If you want run more than one browser (For maxSession uncomment also browser Firefox and IE )
    //maxSessions: 3,

    allScriptsTimeout: 50000,
    rootElement: 'body'
};
