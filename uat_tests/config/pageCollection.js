'use strict';
var pageCollection = (function () {
    var weather = require('../tests/page/weatherPage');
    return {
        weather: weather
    };
}());
module.exports = pageCollection;

