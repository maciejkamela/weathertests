/* jshint node: true */
'use strict';
var basicSupport = (function () {
    return {
        getLocatorWithArguments: function (args, elementPath) {
            var n;
            for (var i = 0; i < args.length; i++) {
                n = i + 1;
                elementPath = elementPath.replace('arg' + n, args[i]);
            }
            return elementPath;
        }
    };
}());

module.exports = basicSupport;