/* jshint node: true */
'use strict';
var Page = require('astrolabe').Page,
    world = require('../../config/world.js');

var self = Page.create({
    url: {value: '/'},
    smallMapTemp: {
        get: function () {
            return dvr.findElement(self.by.xpath('//*[@id="box-info-tab3"]/div[1]/div/p[2]/span'));
        }
    },
    tomorrowTemp: {
        get: function () {
            return dvr.findElement(self.by.xpath("//*[@class='items poland']/*[last()]//*[@class='info']/strong"));
        }
    },
    tomorrowWeatherIcon: {
        get: function () {
            return dvr.findElement(self.by.xpath("//*[@class='items poland']/*[last()]//*[@class='ico']"));
        }
    },
    weatherIcon: {
        value: function (cityName) {
            var icon = world.support.getLocatorWithArguments([cityName], "//a[contains(@href,'arg1') and @class='map-city']");
            return dvr.findElement(self.by.xpath(icon));
        }
    },

    tomorrowBtn: {
        get: function () {
            return dvr.findElement(self.by.css('.map-control-buttons ul :nth-child(3)'));
        }
    },
    searchBox: {
        get: function () {
            return dvr.findElement(self.by.css('#formSearch-term'));
        }
    },
    cityTemperature: {
        value: function (cityName) {
            var city = world.support.getLocatorWithArguments([cityName], "//a/p[.= 'arg1']/preceding-sibling::p");
            return dvr.findElement(self.by.xpath(city));
        }
    },
    getCityTemperatureFromMainMap: {
        value: function (city) {
            return self.cityTemperature(city).getText();
        }
    },
    getCityTemperatureFromSmallMap: {
        value: function () {
            return self.smallMapTemp.getText();
        }
    },
    clickTomorrowBtn: {
        value: function () {
            return self.tomorrowBtn.click();
        }
    },
    clickCityName: {
        value: function (cityName) {
            return self.weatherIcon(cityName).click();
        }
    },
    getTomorrowTemp: {
        value: function () {
            return self.tomorrowTemp.getText();
        }
    },
    getWeatherIconSrc: {
        value: function (cityName) {
            return self.weatherIcon(cityName).getAttribute('title');
        }
    },
    getTomorrowWeatherIconSrc: {
        value: function () {
            return self.tomorrowWeatherIcon.getAttribute('title');
        }
    },
    searchNewCity: {
        value: function (city) {
            console.log('************** dupa');
            //browser.pause();
            return self.searchBox
                //.clear()
                .sendKeys(city);
        }
    },
    draggableElement: {
       get: function () {
           return dvr.findElement(self.by.id('drag1'));
       }
    },
    dragAndDrop: {
        value: function () {
            //return browser.actions().dragAndDrop(self.draggableElement, {x: 200, y: 120}).mouseUp().perform();
            return  browser.actions().
            mouseMove(self.draggableElement, {x: 303, y: 380}).
            mouseDown().
            mouseMove(self.draggableElement, {x: 100, y: 100}).
            mouseUp().
            perform();
        }
    }
});
module.exports = self;
