/* jshint node: true */
'use strict';
var world = require('../../config/world.js'),
    pages = require('../../config/pageCollection.js'),
    config = require('../../config/protractor.cucumber.conf.js');

var weatherSteps = function () {
    this.setDefaultTimeout(60000);
    var weather = pages.weather,
        cityTemperatureOnMainPage,
        cityName,
        cityTempOnSmallMap,
        tomorrowTemp,
        CELSIUS = '°C',
        weatherIconOnMainPage;
    this.Given(/^I am on the main page$/, function (callback) {
        browser.get(config.config.baseUrl)
            .then(function () {
                return callback();
            });
    });
    this.When(/^I want to check today's temperature for "([^"]*)" on the main map$/, function (city, callback) {
        cityName = city;
        weather.getCityTemperatureFromMainMap(city)
            .then(function (temp) {
                cityTemperatureOnMainPage = temp;
            })
            .then(function () {
                callback();
            });
    });
    this.Then(/^I should see the same temperature on the small map$/, function (callback) {
        weather.getCityTemperatureFromSmallMap()
            .then(function (retrievedTemp) {
                cityTempOnSmallMap = retrievedTemp;
            })
            .then(function () {
                var full = cityTempOnSmallMap + ' ' + CELSIUS;
                world.expect(full).equal(cityTemperatureOnMainPage, 'Wrong city\'s temperature');
            })
            .then(function () {
                callback();
            });
    });
    this.When(/^I want to check weather icon for "([^"]*)" on the main map$/, function (city, callback) {
        city = city.toLowerCase();
        weather.getWeatherIconSrc(city)
            .then(function (icon) {
                weatherIconOnMainPage = icon;
            })
            .then(function () {
                callback();
            });
    });

    this.When(/^I want to check tomorrow's temperature for "([^"]*)" on the main map$/, function (city, callback) {
        weather.clickTomorrowBtn()
            .then(function () {
                return weather.getCityTemperatureFromMainMap(city);
            })
            .then(function (temp) {
                cityTemperatureOnMainPage = temp;
                console.log('***********', cityTemperatureOnMainPage);
            })
            .then(function () {
                city = city.toLowerCase();
                return weather.clickCityName(city);
            })
            .then(function () {
                callback();
            });
    });
    this.Then(/^I should see the same temperature and icon on the detailed weather table$/, function (callback) {
        weather.getTomorrowTemp()
            .then(function (temp) {
                tomorrowTemp = temp;
            })
            .then(function () {
                var full = tomorrowTemp + ' ' + CELSIUS;
                world.expect(full).equal(cityTemperatureOnMainPage, 'Wrong city\'s temperature');
                world.expect(weather.getTomorrowWeatherIconSrc()).to.eventually.equal(weatherIconOnMainPage, 'Wrong weather icon.');
            })
            .then(function () {
                callback();
            });
    });

    this.When(/^I search for "([^"]*)"$/, function (city, callback) {
        weather.searchNewCity(city)
            .then(function () {
                callback();
            });
    });
    this.When(/^I am dropping page element$/, function (callback) {
    weather.dragAndDrop()
        .then(function () {
            browser.pause();
        })
        .then(function () {
            callback();
        });

    });


};
module.exports = weatherSteps;
