/* jshint node: true */
'use strict';
module.exports = function JsonOutputHook() {
  var Cucumber = require('cucumber'),
   JsonFormatter = Cucumber.Listener.JsonFormatter(),
   fs = require('fs'),
   config = require('../../config/protractor.cucumber.conf.js').config,
   deleteFolderRecursive = function(path) {
    if(fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function (file) {
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  };

  deleteFolderRecursive(config.jsonReportsDirOutput);

  JsonFormatter.log = function (json) {
    var path = config.jsonReportsDirOutput + '/report_'+ (new Date().getTime()) +'.json',
        directories = config.jsonReportsDirOutput.split("/"),
        currentDir = '';


    for (var i = 0; i < directories.length; i++) {
        currentDir +=  directories[i];
        if (!fs.existsSync(currentDir)) {
            fs.mkdirSync(currentDir);
        }
        currentDir += '/';
    }

    fs.writeFile(path, json, function (err) {
      if (err) {
        throw err;
      }
    });
  };

  this.registerListener(JsonFormatter);
};