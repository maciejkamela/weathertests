@pogoda
Feature: As a Omenaa Mensah I want to check temperature for my favourite city

  Scenario: Checking city forecast temperature for city
    Given I am on the main page
    When I want to check today's temperature for "Warszawa" on the main map
    Then I should see the same temperature on the small map

  Scenario: Checking tomorrow's city forecast temperature for city
    Given I am on the main page
    When I want to check weather icon for "Kielce" on the main map
    And I want to check tomorrow's temperature for "Kielce" on the main map
    Then I should see the same temperature and icon on the detailed weather table

  Scenario: Checking tomorrow's city forecast temperature for city
    Given I am on the main page
    When I search for "Olsztyn"

  Scenario: Drag and drop
    Given I am on the main page
    When I am dropping page element
