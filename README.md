# Automatic tests for twojapogoda.pl.

***
# Description
Automatic tests written in [Protractor](https://angular.github.io/protractor/#/tutorial).
# Requirements

* [NodeJS](http://nodejs.org/)
*  [GruntJS](http://gruntjs.com/)
* [Selenium-standalone](https://www.npmjs.com/package/selenium-standalone)

# Installation and running test

1.  Install NodeJS and NPM

    Firstly  install nodejs
  
    Ubuntu
    http://richardhsu.net/2013/10/19/installing-nodejs-npm-on-ubuntu-13-10/
    
    Mac
    http://coolestguidesontheplanet.com/installing-node-js-on-osx-10-10-yosemite/
    
    Windows
    http://nodejs.org/
    
2.  Install Selenium-Standalone 

   * npm install selenium-standalone@latest -g
   
   * selenium-standalone install
    
3.  Install grunt

    * npm install -g grunt
    * npm install -g grunt-cli

4. Running tests:

    * npm install
    * run selenium (selenium-standalone start)
    * grunt protractor
